赛车游戏
=================

纯js实现的赛车游戏：可定制多种赛事。

测试版可见：随机赛道，7辆电脑车。

 * [http://sodis.github.io/racing-js/](http://sodis.ga/racing-js)

创建赛事
---------------

    // 新建赛道
    var track = new Track('Test Track');

    // 赛道起始线：白色
    track.addSegments(1, { color: 0xEEEEEE });

    // add 80 segments (a stright)
    track.addSegments(80);

    // 右拐，带上坡度
    track.addSegments(20, { curve: -0.5, hill: -0.1 });

    // 加路边风景：在第10点右侧加棵树add a tree in the 10th segment at 500px (right)
    track.addObject(10, 'assets/tree.png', 500);

    // 加入玩家的车
    var playerCar = new Car('assets/sidewinder.png');

    // 加入电脑的车
    var cpuCar = new Car('assets/sidewinder.png');

    // 新建比赛
    var race = new Race(track, [playerCar, cpuCar]);

    // create a render to show the race in the screen
    var render = new RaceRender(race);

    // 加入玩家的键盘控制
    var keyboard = new KeyboardControl(playerCar);

    // 加入电脑的键盘控制
    var cpuControl = new BasicCPUControl(cpuCar);

待做的功能：
----------

- [ ] 单元测试；
- [ ] 提升性能；
- [ ] 提升比赛；
- [ ] 提升设计。

怎么参与？？？
------------------

 1. `npm run watch` to generate a development version of main js;
 2. Use the index.html as your sandbox;
 3. `npm run build` to generate the final version of main js.
